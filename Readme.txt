This is a canadian cross GCC toolchain (multilib). Built by the fast_io library's author

Unix Timestamp:1633772554.10327562
UTC:2021-10-09T09:42:34.10327562Z

fast_io:
https://github.com/tearosccebe/fast_io.git

build	:	x86_64-linux-gnu
host	:	x86_64-w64-mingw32
target	:	riscv64-elf

riscv64-elf-g++ -v
Using built-in specs.
COLLECT_GCC=riscv64-elf-g++
COLLECT_LTO_WRAPPER=d:/x86_64-windows-gnu/riscv64-elf/bin/../libexec/gcc/riscv64-elf/12.0.0/lto-wrapper.exe
Target: riscv64-elf
Configured with: ../../../../gcc/configure --disable-nls --disable-werror --with-newlib --disable-libstdcxx-verbose --host=x86_64-w64-mingw32 --target=riscv64-elf --prefix=/home/cqwrteur/canadian/riscv64-elf --enable-multilib --enable-languages=c,c++ --with-pkgversion=cqwrteur --with-gxx-libcxx-include-dir=/home/cqwrteur/canadian/riscv64-elf/riscv64-elf/include/c++/v1
Thread model: single
Supported LTO compression algorithms: zlib
gcc version 12.0.0 20211009 (experimental) (cqwrteur)
